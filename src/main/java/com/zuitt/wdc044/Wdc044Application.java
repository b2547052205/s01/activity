package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

// "@SpringBootApplication" this is called as "Annotations" mark.
// Annotations are used to provide supplemental information about the program.
// These are used to manage and configure the behavior of the framework.
// Spring Boot scans for classes in its class path upon running and assigns behaviors on them based on their annotations.

// To specify the main class of the Spring boot application.
@SpringBootApplication
// Tells the spring boot that this will handle endpoints for web request.
@RestController
public class Wdc044Application {

	public static void main(String[] args) {
		// This method starts the whole Spring Framework.
		SpringApplication.run(Wdc044Application.class, args);
	}


	// This is used for mapping HTTP GET requests.
	@GetMapping("/hello")
	// "@RequestParam" is used to extract query parameters, form parameters, and even files from the request.
	//name = john; Hello john.
	//name = World; Hello World.
	// To append the URL with a name parameter we do the following:
	//http:localhost:8080/hello?name=john
	//"?" means the start of the parameters followed by the "key=value" pair.

	public String hello(@RequestParam(value = "name", defaultValue = "World") String name ) {
		return String.format("Hello %s!", name);
	}

	@GetMapping("/greetings")
	public String greetings(@RequestParam(value = "greet", defaultValue = "World") String greet) {
		return String.format("Good evening, %s! Welcome to Batch 254!", greet);
	}

	//Spring Boot S01 Activity
	@GetMapping("/hi")
	public String hi(@RequestParam(value = "user", defaultValue = "user") String user) {
		return String.format("hi %s!", user);
	}

	// Stretch goals
	//localhost:8080/nameAge?name=nameVal&age=ageVal
	@GetMapping("/nameAge")
	public String nameAge(@RequestParam(value = "name", defaultValue = "Juan") String name, @RequestParam(value = "age", defaultValue = "18") String age){
		return String.format("Hello %s! Your age is %s.", name, age);
	}


}
